package de.hhbk;

import org.hibernate.Session;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.List;

class GenericDao<T> {
    public T load(Class<T> clazz, int id) {
        T r = null;
        Session session = HibernateManager.getSessionFactory().openSession();
        try {
            r = session.get(clazz, id);
        } catch (Exception e) {
            System.out.println(">>> ERROR Dao.load(): " + e);
        } finally {
            try {
                session.close();
            } catch (Exception e) { }
            return r;
        }
    }

    public Serializable save(T t) {
        Serializable r = null;
        Session session = HibernateManager.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            r = session.save(t);
            session.getTransaction().commit();
            System.out.println(">>> Saved successfully: " + t);
        } catch (Exception e) {
            System.out.println(">>> ERROR Dao.save(): " + e.getMessage());
        } finally {
            try {
                session.close();
            } catch (Exception e) { }
            return r;
        }
    }

    public void delete(T t) {
        boolean r = false;
        Session session = HibernateManager.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            session.delete(t);
            session.flush();
            session.getTransaction().commit();
            System.out.println(">>> Deleted successfully: " + t);
        } catch (Exception e) {
            System.out.println(">>> ERROR Dao.delete(): " + e.getMessage());
        } finally {
            try {
                session.close();
            } catch (Exception e) { }
        }
    }

    public List<T> loadAll(Class <T> clazz) {
        List results = null;
        T r = null;
        Session session = HibernateManager.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery("from " + clazz.getName());
            results = query.list();
            session.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(">>> ERROR Dao.loadAll(): " + e);
        } finally {
            try {
                session.close();
            } catch (Exception e) { }
            return results;
        }
    }

    public Serializable saveAll(List<T> t) {
        // TODO how to return a Serializable if we persists the individual objects?
        Serializable r = null;
        Session session = HibernateManager.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            t.stream().forEach(o -> session.save(o));
            session.getTransaction().commit();
            System.out.println(">>> Batch Saved successfully: " + t);
        } catch (Exception e) {
            System.out.println(">>> ERROR Dao.saveAll(): " + e.getMessage());
        } finally {
            try {
                session.close();
            } catch (Exception e) { }
            return r;
        }
    }

    public void deleteAll(List<T> t) {
        boolean r = false;
        Session session = HibernateManager.getSessionFactory().openSession();
        try {
            session.beginTransaction();
            t.stream().forEach(o -> session.delete(o));
            session.flush();
            session.getTransaction().commit();
            System.out.println(">>> Batch Deleted successfully: " + t);
        } catch (Exception e) {
            System.out.println(">>> ERROR Dao.deleteAll(): " + e.getMessage());
        } finally {
            try {
                session.close();
            } catch (Exception e) { }
        }
    }
}
