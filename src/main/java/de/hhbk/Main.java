package de.hhbk;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Rennwagen porsche =  new Rennwagen(1, "Rost", "Porsche", 750, 6,
                90F, 45F);
        Rennwagen ferrari =  new Rennwagen(2, "Schimmel", "Ferrari", 850, 8,
                92F, 88F);
        Fahrer kevin = new Fahrer(1, "Kevin", "der Kleinste", 17, 2);
        Fahrer kurt = new Fahrer(2, "Kurt", "ohne Helm und ohne Gurt", 69, Integer.MAX_VALUE);

        GenericDao<Rennwagen> rennwagenDao = new GenericDao<>();
        GenericDao<Fahrer> fahrerDao = new GenericDao<>();

        // save & load
        rennwagenDao.save(porsche);
        rennwagenDao.save(ferrari);
        Rennwagen loaded_r1 = rennwagenDao.load(Rennwagen.class, porsche.getId());
        System.out.println(">>> Loaded Rennwagen: " + loaded_r1);

        // delete
        rennwagenDao.delete(loaded_r1);
        Rennwagen deleted_r1 = rennwagenDao.load(Rennwagen.class, porsche.getId());
        System.out.println(">>> Deleted Rennwagen should be null: " + deleted_r1);

        // saveAll
        ArrayList<Fahrer> fahrerList = new ArrayList<>(Arrays.asList(kurt, kevin));
        fahrerDao.saveAll(fahrerList);
        Fahrer loaded_f1 = fahrerDao.load(Fahrer.class, kurt.getFahrerId());
        System.out.println(">>> Loaded Fahrer: " + loaded_f1);

        // loadAll
        List<Fahrer> alleFahrer = fahrerDao.loadAll(Fahrer.class);
        System.out.println(">>> Loaded all Fahrer objects: " + alleFahrer);
        List<Rennwagen> alleRennwagen = rennwagenDao.loadAll(Rennwagen.class);
        System.out.println(">>> Loaded all Rennwagen objects: " + alleRennwagen);

        // delete
        fahrerDao.deleteAll(alleFahrer);
        System.out.println(">>> Batch deleted all Fahrer objects: " + alleFahrer);
        alleFahrer = fahrerDao.loadAll(Fahrer.class);
        System.out.println(">>> Batch Deleted Fahrer should be empty: " + alleFahrer);
    }
}
